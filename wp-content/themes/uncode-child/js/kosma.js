(function($) {
	"use strict";

	var selectedLang = 'en'; // get default by setting.
	function getCookie(cname) {
		var name = cname + '=';
		var ca = document.cookie.split(';');
		for(var i = 0; i < ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) == ' ') {
				c = c.substring(1);
			}
			if (c.indexOf(name) == 0) {
				return c.substring(name.length, c.length);
			}
		}
		return '';
	}

	function setCookie(cname, cvalue, exdays) {
		var d = new Date();
		d.setTime(d.getTime() + (exdays*24*60*60*1000));
		var expires = 'expires='+ d.toUTCString();
		document.cookie = cname + '=' + cvalue + '; path=/;' + expires;
	}

	function getHostUrl() {
		var http = location.protocol;
		var slashes = http.concat("//");
		return slashes.concat(window.location.hostname);
	}

	$(document).ready(function() {
		

		$(".search-container-inner .fa-search3").on('click',function () {
			$(".search-container form").submit();
		});
		$(".kosma_button").on('click',function () {
			window.history.back();
		});
		//
		var cartLink=$(".cart_list a.wc-forward").attr('href');
		var quantity=$(".cart-icon-container .badge").html();
		if(quantity!="") var quantity='<span class="badge">'+quantity+'</span>';
		$(".logo-container").append('<div class="kosma-cart"><a href="'+cartLink+'"><i class="fa fa-bag"></i></a></div>');

		//Cart page
		// $( document.body ).on( 'wc_fragments_refreshed',function () {
		//     location.reload();
		// });

		//Return
		$(".return-checkbox").on('change',function () {
			var quantity=$("#return-product-"+$(this).data('product_id')+$(this).data('price'));
			if($(this).prop('checked')) quantity.enable(true);
			else quantity.enable(false);
		});
		$("#return-submit").on('click',function () {
			var data={};
			data.order=$("#return-order-id").val();
			data.email=$("#return-email").val();
			data.message=$("#return-message").val();
			data.products=[];

			$(".return-checkbox").each(function (index) {
				if($(this).prop('checked')) {
					var quantity=$("#return-product-"+$(this).data('product_id')+$(this).data('price'));
					var product={
						id:$(this).data("product_id"),
						variation_id:$(this).data("variation_id"),
						name:$(this).data("name"),
						quantity:quantity.val()
					};
					data.products.push(product);
				}
			});
			var input = $("<input>")
				.attr("type", "hidden")
				.attr("name", "returndata").val(JSON.stringify(data));
			$('#return-form').append($(input));
			$('#return-form').submit();
		});
		var kosma_images=$("#kosma-data-holder").data("images");
		$(".tmb").mouseenter(function () {
			var curr=$(this).find("img");
			if (kosma_images && kosma_images.length) {
				curr.data("oldSrc",curr.attr("src"));
				curr.attr("src",kosma_images[$(this).index()]);
			}
		}).mouseleave(function () {
			var curr=$(this).find("img");
			curr.attr("src",curr.data("oldSrc"));
		});

		//Show cart after adding to cart
		if($(".woocomerce-success").text()!=""){
			setTimeout(
				function () {
					$(".navbar-nav-last .menu-smart").smartmenus('itemActivate', $('.uncode-cart  .uncode-cart-dropdown'));
					$('.uncode-cart .uncode-cart-dropdown').show();
				},500
			);
		}
		$(".kosma-color-item").on('click',function () {
			$(".kosma-color-item").each(function () {
				$(this).removeClass('selected');
			});
			$(this).addClass('selected');
		});

		$(document).on('click', '#select-language', function() {
			var url = getHostUrl();
			setCookie('kosma-choose-language', selectedLang, 1000, '/');
			if (selectedLang !== 'en') {
				url = getHostUrl() + '/' + selectedLang;
			} 
			window.location = url;
		});

		$(document).on('click', '.enflag', function(e) {
			e.preventDefault();
			var url = getHostUrl();
			setCookie('kosma-choose-language', 'en', 1000, '/');
			window.location = url;
		});

		$(document).on('click', '.vnflag', function(e) {
			e.preventDefault();
			var url = getHostUrl();
			setCookie('kosma-choose-language', 'vn', 1000, '/');
			window.location = url + '/vi/';
		});

		$(document).on('change', '#kosma-choose-language', function(e) {
			selectedLang = e.target.value;
		});

	});
})(jQuery);
