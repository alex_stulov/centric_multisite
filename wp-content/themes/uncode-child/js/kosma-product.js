jQuery(function($) {
    $(document).ready(function() {
        var data=$("#kosma_add_product_form").data('product_variations');
        var kosma_size;
        var kosma_color;
        $(".kosma_size").on('click',function(){
            //Set active
            $(".kosma_size").each(function(){
                if($(this).hasClass("active")) {
                    $(this).removeClass("active");
                }
            });
            $(this).addClass("active");
            kosma_size=$(this).data('size');
            updateVariation();

         });
        $(".kosma_color").on('click',function(){
            //Set active
            $(".kosma_color").each(function(){
                if($(this).hasClass("active")) {
                    $(this).removeClass("active");
                }
            });
            $(this).addClass("active");
            kosma_color=$(this).data('color');
            //Check variations
            for(var i=0;i<data.length;i++){
                if($(this).data('color')==data[i].attributes.attribute_pa_color) {
                    $('.woocommerce-images>a>img').attr('src',data[i].image_link);
                }
                if(kosma_color==data[i].attributes.attribute_pa_color&&kosma_size==data[i].attributes.attribute_pa_size){
                    $('.variation_id').val(data[i].variation_id);
                }
            }
            updateVariation();
         });

        
        // active it if My want to focus to first varriation product
        // $(function () {
        //     $('#picker_pa_color .swatch-wrapper').first().trigger('click');
        //     $('#picker_pa_size .swatch-wrapper').first().trigger('click');
        // });

        function updateVariation(){
            for(var i=0;i<data.length;i++){
                if(data[i].attributes.attribute_pa_color==""){
                    if(data[i].attributes.attribute_pa_size==kosma_size||data[i].attributes.attribute_pa_size=="")
                        $('.variation_id').val(data[i].variation_id);
                }else{
                    if(data[i].attributes.attribute_pa_size==""){
                        if(data[i].attributes.attribute_pa_color==kosma_color)
                            $('.variation_id').val(data[i].variation_id);
                    }else{
                        if(data[i].attributes.attribute_pa_color==kosma_color&&data[i].attributes.attribute_pa_size==kosma_size)
                            $('.variation_id').val(data[i].variation_id);
                    }
                }
            }
        }

        function getParameterByName(name, url) {
            if (!url) {
              url = window.location.href;
            }
            name = name.replace(/[\[\]]/g, "\\$&");
            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, " "));
        }

        function getUrlParameter(sParam) {
            var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : sParameterName[1];
                }
            }
        }
       // get filters preset from the category page
        var presetSize = getUrlParameter('size-filter'),
            presetColor = getUrlParameter('color-filter');
        if (presetSize != undefined) {
            $("div[data-attribute='pa_size']").each(function(){
                if ($(this).attr('data-value') == presetSize && !$(this).hasClass('selected')) {
                    $(this).find("a").trigger('click');
                }
            });
        }
        if (presetColor != undefined) {
            $("div[data-attribute='pa_color']").each(function(){
                if ($(this).attr('data-value') == presetColor && !$(this).hasClass('selected')) {
                    $(this).find("a").trigger('click');
                }
            });
        }
    });
});