<?php
/**
 * Output a single payment method
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/payment-method.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

?>
<li class="wc_payment_method payment_method_<?php echo $gateway->id; ?>">
	<input id="payment_method_<?php echo $gateway->id; ?>"
           type="radio"
           class="input-radio"
           name="payment_method"
           value="<?php echo esc_attr( $gateway->id ); ?>"
                  <?php checked( $gateway->chosen, true ); ?>
           data-order_button_text="<?php echo esc_attr( $gateway->order_button_text ); ?>" />

	<label for="payment_method_<?php echo $gateway->id; ?>">
			<!-- HARDCODED VALUES. SOMEHOW WORDPRESS EDIT SAVED FIELD FROM ADMIN PANEL AND REMOVE ALL 'STRANGE' SYMBOLS-->
			<?php if ($gateway->id == 'bacs') : ?>
				Chuyển khoản
			<?php elseif ($gateway->id == 'EG_Onepay_Gateway') : ?>
				Thanh toán bằng thẻ ATM nội địa
			<?php elseif ($gateway->id == 'EG_Onepay_Gateway_US') : ?>
				Thanh toán bằng thẻ Visa, Master, etc.
			<?php else : ?>
				<?php echo $gateway->get_title(); ?>
			<?php endif; ?>
	</label>
	<?php if ( $gateway->has_fields() || $gateway->get_description() ) : ?>
		<div class="payment_box payment_method_<?php echo $gateway->id; ?>" <?php if ( ! $gateway->chosen ) : ?>style="display:none;"<?php endif; ?>>
			<?php $gateway->payment_fields(); ?>
			<?php if ( $gateway->id == 'EG_Onepay_Gateway' ) : ?>
				<p><img src="https://www.centricwear.com/wp-content/uploads/2016/04/OPATM.png" /></p>
			<?php endif; ?>
			<?php if ( $gateway->id == 'EG_Onepay_Gateway_US' ) : ?>
				<img src="https://www.centricwear.com/wp-content/uploads/2016/04/US.png"/>
			<?php endif; ?>
		</div>
	<?php endif; ?>
</li>
