<?php
/**
 * Review order table
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/review-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
if (defined('DOING_AJAX') && DOING_AJAX) {
	$packages = WC()->shipping->get_packages();

	$response=[];
	$ship_method="";

	foreach ($packages as $i => $package) {
		$chosen_method = isset(WC()->session->chosen_shipping_methods[$i]) ? WC()->session->chosen_shipping_methods[$i] : '';
		$product_names = array();

		if (sizeof($packages) > 1) {
			foreach ($package['contents'] as $item_id => $values) {
				$product_names[] = $values['data']->get_title() . ' &times;' . $values['quantity'];
			}
		}
		$available_methods = $package['rates'];
		foreach ($available_methods as $method) {
			if ((string)$method->id === (string)$chosen_method) {
				$label="";
				if ( $method->cost > 0 ) {
					if ( WC()->cart->tax_display_cart == 'excl' ) {
						$label .=wc_price( $method->cost );
						if ( $method->get_shipping_tax() > 0 && WC()->cart->prices_include_tax ) {
							$label .= ' <small class="tax_label">' . WC()->countries->ex_tax_or_vat() . '</small>';
						}
					} else {
						$label .= wc_price( $method->cost + $method->get_shipping_tax() );
						if ( $method->get_shipping_tax() > 0 && ! WC()->cart->prices_include_tax ) {
							$label .= ' <small class="tax_label">' . WC()->countries->inc_tax_or_vat() . '</small>';
						}
					}
				}else $label=wc_price(0);
				$ship_method= '<th >Shipping</th>
	                    <th colspan="2">'.$method->get_label().'</th>
	                    <td >'.$label.'</td>';
			}
		}
	}
	$value = '<strong>' . WC()->cart->get_total() . '</strong> ';

// If prices are tax inclusive, show taxes here
	if ( wc_tax_enabled() && WC()->cart->tax_display_cart == 'incl' ) {
		$tax_string_array = array();

		if ( get_option( 'woocommerce_tax_total_display' ) == 'itemized' ) {
			foreach ( WC()->cart->get_tax_totals() as $code => $tax )
				$tax_string_array[] = sprintf( '%s %s', $tax->formatted_amount, $tax->label );
		} else {
			$tax_string_array[] = sprintf( '%s %s', wc_price( WC()->cart->get_taxes_total( true, true ) ), WC()->countries->tax_or_vat() );
		}

		if ( ! empty( $tax_string_array ) ) {
			$taxable_address = WC()->customer->get_taxable_address();
			$estimated_text  = WC()->customer->is_customer_outside_base() && ! WC()->customer->has_calculated_shipping()
				? sprintf( ' ' . __( 'estimated for %s', 'woocommerce' ), WC()->countries->estimated_for_prefix( $taxable_address[0] ) . WC()->countries->countries[ $taxable_address[0] ] )
				: '';
			$value .= '<small class="includes_tax">' . sprintf( __( '(includes %s)', 'woocommerce' ), implode( ', ', $tax_string_array ) . $estimated_text ) . '</small>';
		}
	}

	$response['shipinfo']=$ship_method;
	$response['total']=$value;
	echo json_encode($response);
}

?>
