<?php
/**
 * Checkout Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see        http://docs.woothemes.com/document/template-structure/
 * @author        WooThemes
 * @package    WooCommerce/Templates
 * @version     2.3.0
 */

if (!defined('ABSPATH')) {
    exit;
}

wc_print_notices();
?>

<div class="post-content">
    <div data-parent="true" class="row-container boomapps_vcrow" data-section="0">
        <div class="row limit-width row-parent kosma-checkout-layout" data-imgready="true">
            <div class="row-inner">
                <div
                    class="pos-top pos-center align_left column_parent col-lg-6 boomapps_vccolumn single-internal-gutter">
                    <div class="uncol style-light">
                        <div class="uncoltable">
                            <div class="uncell  boomapps_vccolumn no-block-padding">
                                <div class="uncont">
                                    <?php

                                    do_action('woocommerce_before_checkout_form', $checkout);

                                    // If checkout registration is disabled and not logged in, the user cannot checkout
                                    if (!$checkout->enable_signup && !$checkout->enable_guest_checkout && !is_user_logged_in()) {
                                        echo apply_filters('woocommerce_checkout_must_be_logged_in_message', __('You must be logged in to checkout.', 'woocommerce'));
                                        return;
                                    }

                                    ?>

                                    <form name="checkout" method="post" class="checkout woocommerce-checkout kosma-checkout-form"
                                          action="<?php echo esc_url(wc_get_checkout_url()); ?>" enctype="multipart/form-data">

                                        <?php if (sizeof($checkout->checkout_fields) > 0) : ?>

                                            <?php do_action('woocommerce_checkout_before_customer_details'); ?>

                                            <div class="col2-set" id="customer_details">
                                                <div class="col-1">
                                                    <?php do_action('woocommerce_checkout_billing'); ?>
                                                </div>

                                                <div class="col-2">
                                                    <?php do_action('woocommerce_checkout_shipping'); ?>
                                                    <?php do_action('kosma_load_shipping_method'); ?>
                                                    <table class="shop_table woocommerce-checkout-review-order-table-kosma" style="display: none"></table>
                                                </div>
                                            </div>

                                            <?php do_action('woocommerce_checkout_after_customer_details'); ?>



                                        <?php endif; ?>

<!--                                        <h3 id="order_review_heading">--><?php //_e('Your order', 'woocommerce'); ?><!--</h3>-->

                                        <?php do_action('woocommerce_checkout_before_order_review'); ?>

                                        <div id="order_review" class="woocommerce-checkout-review-order">
                                            <h3><?php _e( '3. Payment', 'woocommerce' ); ?></h3>
                                            <?php do_action('woocommerce_checkout_order_review'); ?>
                                        </div>
                                        <?php do_action( 'woocommerce_review_order_after_order_total' ); ?>

                                        <?php do_action('woocommerce_checkout_after_order_review'); ?>

                                    </form>

                                    <?php do_action('woocommerce_after_checkout_form', $checkout); ?>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div
                    class="pos-top pos-center align_left column_parent col-lg-6 boomapps_vccolumn single-internal-gutter">
                    <div class="uncol style-light">
                        <div class="uncoltable">
                            <div class="uncell  boomapps_vccolumn no-block-padding">
                                <div class="uncont">
                                    <table class="shop_table woocommerce-checkout-review-order-table-kosma kosma-ta">
                                        <thead>
                                        <tr>
                                            <th class="product-thumbnail"><?php _e( 'Summary', 'woocommerce' ); ?></th>
                                            <th class="product-quantity"></th>
                                            <th class="product-name"></th>
                                            <th class="product-total"><?php _e( 'Total', 'woocommerce' ); ?></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        do_action( 'woocommerce_review_order_before_cart_contents' );

                                        foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
                                            $_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );

                                            if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_checkout_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
                                                ?>
                                                <tr class="<?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">
                                                    <td class="product-thumbnail">
                                                        <?php
                                                        $thumbnail = apply_filters('woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key);

                                                        if (!$_product->is_visible()) {
                                                            echo $thumbnail;
                                                        } else {
                                                            printf('<a href="%s">%s</a>', esc_url($_product->get_permalink($cart_item)), $thumbnail);
                                                        }
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?php echo apply_filters( 'woocommerce_checkout_cart_item_quantity', ' <strong class="product-quantity">' . sprintf( '&times; %s', $cart_item['quantity'] ) . '</strong>', $cart_item, $cart_item_key ); ?>
                                                    </td>
                                                    <td class="product-name">
                                                        <?php echo apply_filters( 'woocommerce_cart_item_name', $_product->get_title(), $cart_item, $cart_item_key ) . '&nbsp;'; ?>

                                                        <?php echo WC()->cart->get_item_data( $cart_item ); ?>
                                                    </td>
                                                    <td class="product-total">
                                                        <?php echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key ); ?>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                        }

                                        do_action( 'woocommerce_review_order_after_cart_contents' );
                                        ?>
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <td colspan="4">
                                                <form class="checkout_coupon_kosma" method="post">

                                                    <p class="form-row form-row-first">
                                                        <input type="text" name="coupon_code" class="input-text" placeholder="<?php esc_attr_e( 'Coupon code', 'woocommerce' ); ?>" id="coupon_code" value="" />
                                                    </p>

                                                    <p class="form-row form-row-last">
                                                        <input type="submit" class="button" name="apply_coupon" value="<?php esc_attr_e( 'Apply Coupon', 'woocommerce' ); ?>" />
                                                    </p>

                                                    <div class="clear"></div>
                                                </form>
                                            </td>

                                        </tr>

                                        <tr class="cart-subtotal">
                                            <th><?php _e( 'Subtotal', 'woocommerce' ); ?></th>
                                            <td></td>
                                            <td></td>
                                            <td><?php wc_cart_totals_subtotal_html(); ?></td>
                                        </tr>

                                        <?php foreach ( WC()->cart->get_coupons() as $code => $coupon ) : ?>
                                            <tr class="cart-discount coupon-<?php echo esc_attr( sanitize_title( $code ) ); ?>">
                                                <th><?php wc_cart_totals_coupon_label( $coupon ); ?></th>
                                                <td></td>
                                                <td></td>
                                                <td><?php wc_cart_totals_coupon_html( $coupon ); ?></td>
                                            </tr>
                                        <?php endforeach; ?>

                                        <?php if ( WC()->cart->needs_shipping() && WC()->cart->show_shipping() ) : ?>
                                            <?php do_action( 'kosma_load_current_shipping_method' ); ?>
                                        <?php endif; ?>

                                        <?php foreach ( WC()->cart->get_fees() as $fee ) : ?>
                                            <tr class="fee">
                                                <th><?php echo esc_html( $fee->name ); ?></th>
                                                <td></td>
                                                <td></td>
                                                <td><?php wc_cart_totals_fee_html( $fee ); ?></td>
                                            </tr>
                                        <?php endforeach; ?>

                                        <?php if ( wc_tax_enabled() && 'excl' === WC()->cart->tax_display_cart ) : ?>
                                            <?php if ( 'itemized' === get_option( 'woocommerce_tax_total_display' ) ) : ?>
                                                <?php foreach ( WC()->cart->get_tax_totals() as $code => $tax ) : ?>
                                                    <tr class="tax-rate tax-rate-<?php echo sanitize_title( $code ); ?>">
                                                        <th><?php echo esc_html( $tax->label ); ?></th>
                                                        <td></td>
                                                        <td></td>
                                                        <td><?php echo wp_kses_post( $tax->formatted_amount ); ?></td>
                                                    </tr>
                                                <?php endforeach; ?>
                                            <?php else : ?>
                                                <tr class="tax-total">
                                                    <th><?php echo esc_html( WC()->countries->tax_or_vat() ); ?></th>
                                                    <td></td>
                                                    <td></td>
                                                    <td><?php wc_cart_totals_taxes_total_html(); ?></td>
                                                </tr>
                                            <?php endif; ?>
                                        <?php endif; ?>

                                        <?php do_action( 'woocommerce_review_order_before_order_total' ); ?>

                                        <tr class="order-total">
                                            <th><?php _e( 'Total', 'woocommerce' ); ?></th>
                                            <td></td>
                                            <td></td>
                                            <td id="kosma-total"><?php wc_cart_totals_order_total_html(); ?></td>
                                        </tr>

                                        <?php //do_action( 'woocommerce_review_order_after_order_total' ); ?>

                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <script id="script-118906"
                        type="text/javascript">UNCODE.initRow(document.getElementById("script-118906"));</script>
            </div>
        </div>
    </div>
</div>
