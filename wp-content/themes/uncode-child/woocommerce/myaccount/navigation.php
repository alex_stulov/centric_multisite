<?php
/**
 * My Account navigation
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/navigation.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woothemes.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.6.0
 */

if (!defined('ABSPATH')) {
    exit;
}

do_action('woocommerce_before_account_navigation');
?>

<div data-parent="true" class="row-container boomapps_vcrow" data-section="0">
    <div class="row limit-width row-parent kosma-my-account" data-imgready="true">
        <div class="row-inner">
            <div class="pos-top pos-center align_left column_parent col-lg-3 boomapps_vccolumn single-internal-gutter">
                <div class="uncol style-light">
                    <div class="uncoltable">
                        <div class="uncell  boomapps_vccolumn no-block-padding">
                            <div class="uncont">
                                <nav class="woocommerce-MyAccount-navigation">
                                    <ul>
                                        <?php
                                        //Change order
                                        $kosma_menu = [];
                                        $kosma_menu['edit-account'] = __("My account", "woocommerce");
                                        $kosma_menu['orders'] = __("My orders", "woocommerce");
                                        $kosma_menu['payment-methods'] = __("Payment Methods", "woocommerce");
                                        $kosma_menu['edit-address'] = __("Addresses", "woocommerce");
                                        $kosma_menu['customer-logout'] = __("Logout", "woocommerce");

                                        foreach (wc_get_account_menu_items() as $endpoint => $label) {
                                            if ($kosma_menu[$endpoint] == null) $kosma_menu[$endpoint] = $label;
                                        }
                                        ?>
                                        <?php foreach ($kosma_menu as $endpoint => $label) : ?>
                                            <li class="<?php echo wc_get_account_menu_item_classes($endpoint); ?>">
                                                <a href="<?php echo esc_url(wc_get_account_endpoint_url($endpoint)); ?>"><?php echo esc_html($label); ?></a>
                                            </li>
                                        <?php endforeach; ?>
                                    </ul>
                                </nav>

                                <?php do_action('woocommerce_after_account_navigation'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


