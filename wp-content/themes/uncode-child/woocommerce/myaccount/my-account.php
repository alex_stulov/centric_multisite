<?php
/**
 * My Account page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/my-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woothemes.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

wc_print_notices();

/**
 * My Account navigation.
 * @since 2.6.0
 */
do_action( 'woocommerce_account_navigation' ); ?>

<div class="pos-top pos-center align_left column_parent col-lg-9 boomapps_vccolumn single-internal-gutter">
	<div class="uncol style-light">
		<div class="uncoltable">
			<div class="uncell  boomapps_vccolumn no-block-padding">
				<div class="uncont">
					<div class="woocommerce-MyAccount-content">
						<?php
						/**
						 * My Account content.
						 * @since 2.6.0
						 */
						do_action( 'woocommerce_account_content' );
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script id="script-465440"
		type="text/javascript">UNCODE.initRow(document.getElementById("script-465440"));</script>
</div>
</div>
</div>
