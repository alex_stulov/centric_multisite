<?php
/**
 * View Order
 *
 * Shows the details of a particular order on the account page.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/view-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woothemes.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}
?>
<p><?php
    printf(
        __( 'Order #%1$s was placed on %2$s and is currently %3$s.', 'woocommerce' ),
        $order->get_order_number(),
        $order->order_date,
        wc_get_order_status_name( $order->get_status() )
    );
    ?></p>
<p style="font-weight: 600;color: black;">Please select the item(s) you wish to return in this order:</p>
<table class="shop_table order_details">
    <thead>
    <tr>
        <th><?php _e('Thumbnail','woocommerce')?></th>
        <th class="product-name"><?php _e( 'Product', 'woocommerce' ); ?></th>
        <th><?php _e( 'Price', 'woocommerce' ); ?></th>
        <th><?php _e( 'Return', 'woocommerce' ); ?></th>
        <th><?php _e( 'Quantity', 'woocommerce' ); ?></th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach( $order->get_items() as $item_id => $item ) {
        $product = apply_filters( 'woocommerce_order_item_product', $order->get_product_from_item( $item ), $item );
        ?>
        <tr class="<?php echo esc_attr( apply_filters( 'woocommerce_order_item_class', 'order_item', $item, $order ) ); ?>">
            <td>
                <img src="<?php  echo wp_get_attachment_thumb_url($product->get_image_id())//wp_get_attachment_image_src( $product->get_image_id(), 'thumbnail');?>">
            </td>
            <td class="product-name">
                <?php
                $is_visible        = $product && $product->is_visible();
                $product_permalink = apply_filters( 'woocommerce_order_item_permalink', $is_visible ? $product->get_permalink( $item ) : '', $item, $order );

                echo apply_filters( 'woocommerce_order_item_name', $product_permalink ? sprintf( '<a href="%s">%s</a>', $product_permalink, $item['name'] ) : $item['name'], $item, $is_visible );
                echo apply_filters( 'woocommerce_order_item_quantity_html', ' <strong class="product-quantity">' . sprintf( '&times; %s', $item['qty'] ) . '</strong>', $item );

                do_action( 'woocommerce_order_item_meta_start', $item_id, $item, $order );

                $order->display_item_meta( $item );
                $order->display_item_downloads( $item );

                do_action( 'woocommerce_order_item_meta_end', $item_id, $item, $order );
                ?>
            </td>
            <td class="product-total">
                <?php echo $order->get_formatted_line_subtotal( $item ); ?>
            </td>
            <td>
                <input type="checkbox" class="return-checkbox" data-product_id="<?php echo $item['product_id']?>" data-variation_id="<?php echo $item['variation_id']?>" data-name="<?php echo $item['name']?>" data-price="<?php echo $item['line_total']?>">
            </td>
            <td>
                <input type="number" id="return-product-<?php echo $item['product_id'].$item['line_total']?>" value="1" min="1" max="<?php echo $item['qty']?>" disabled>
            </td>
        </tr>
    <?php
    }
    ?>
    </tbody>
</table>
<form id="return-form" method="post">
    <table class="shop_table order_details">
        <thead>
        <tr>
            <th class="product-name">Reson for return</th>
        </tr>
        </thead>
        <tbody>
        <tr class="order_item">
            <td>
                <input type="hidden" id="return-order-id" value="<?php echo $order->id?>">
                <input type="hidden" id="return-email" value="<?php echo wp_get_current_user()->user_email?>">
                <textarea id="return-message" placeholder="Help us improve our product by telling us why you want to return the item(s)" style="height: 200px"></textarea>
            </td>
        </tr>

        </tbody>
    </table>
    <a href="#" id="return-submit" class="checkout-button btn btn-default alt wc-forward" style="width: 100%">SUBMIT RETURN REQUEST</a>
</form>
