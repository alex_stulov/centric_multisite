<h5>YOUR RETURN REQUEST FOR ORDER #<?php ?> HAS BEEN SENT! HERE ARE YOUR NEXT STEPS</h5>
<div class="kosma-return-success">
    <p>1. Peel & place the included Return Label on top of your package</p>
    <p>2. Drop the pakage at your nearest UPS location</p>
    <p>3. Once we receive your shipment, we will start processing your refund. Please note refund might take up to 5-7 days business days to show up in your bank account. You can always track the process online at My Orders</p>
    <p>* If you have any question an concern, please contact us at Support@centricwear.com, we are happy to help</p>
</div>
<div class="kosma-back-to-homepage">
    <a href="<?php echo home_url( '/' )?>" class="checkout-button btn btn-default alt wc-forward" style="width: 100%">BACK TO YOUR HOMEPAGE</a>
</div>
