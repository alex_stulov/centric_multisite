<?php
add_action('after_setup_theme', 'uncode_language_setup');
function uncode_language_setup()
{
    load_child_theme_textdomain('uncode', get_stylesheet_directory() . '/languages');
}

//Load css and js
function theme_enqueue_styles()
{
    $production_mode = ot_get_option('_uncode_production');
    $resources_version = ($production_mode === 'on') ? null : rand();
    $parent_style = 'uncode-style';
    $child_style = array('uncode-custom-style');
    wp_enqueue_style($parent_style, get_template_directory_uri() . '/library/css/style.css', array(), $resources_version);
    wp_enqueue_style('child-style', get_stylesheet_directory_uri() . '/style.css', $child_style, $resources_version);

    //Load js
//    if (is_product()) {
//        wp_enqueue_script('kosmaproductscript', get_bloginfo( 'stylesheet_directory' ).'/js/kosma-product.js', array('jquery'));
//    }
    wp_enqueue_script('kosmascript', get_bloginfo( 'stylesheet_directory' ).'/js/kosma.js', array('jquery'));
    //wp_dequeue_script('wc-checkout');
    //wp_enqueue_script( 'wc-checkout', get_bloginfo( 'stylesheet_directory' ). '/woocommerce/assets/js/frontend/checkout.js' , array( 'jquery' ), false, true );
    wp_dequeue_script('wc-cart-fragments');
    wp_enqueue_script( 'wc-cart-fragments', get_bloginfo( 'stylesheet_directory' ). '/woocommerce/assets/js/frontend/cart-fragments.js' , array( 'jquery' ), false, true );wp_dequeue_script('wc-cart');
    wp_dequeue_script('wc-cart');
    wp_enqueue_script( 'wc-cart', get_bloginfo( 'stylesheet_directory' ). '/woocommerce/assets/js/frontend/cart.js' , array( 'jquery' ), false, true );
}
add_action('wp_enqueue_scripts', 'theme_enqueue_styles');

//Add size filter shortcode
function size_filter_shortcode($atts, $content = null)
{
    $terms = get_terms('pa_size', array(
        'hide_empty' => false,
    ));
    $content = "";
    foreach ($terms as $term) {
        $content .= "<div>" . $term->name . "</div>";
    }
    return '<div class="kosma"><div>Size</div>' . $content . '</div>';
}
add_shortcode('size_filter', 'size_filter_shortcode');

//Add color filter shortcode
function color_filter_shortcode($atts, $content = null)
{
    $terms = get_terms('pa_color', array(
        'hide_empty' => false,
    ));
    $content = "";
    foreach ($terms as $term) {
        $content .= "<div>" . $term->name . "</div>";
    }
    return '<div class="kosma"><div>Color</div>' . $content . '</div>';
}
add_shortcode('color_filter', 'color_filter_shortcode');

//Add order by shortcode
function order_by_shortcode($atts, $content = null)
{
    return '<div class="kosma"><div>Size</div>' . $content . '</div>';
}
add_shortcode('order_by', 'order_by_shortcode');

//Add kosma filter shortcod
function kosma_filter_shortcode($atts, $content = null)
{
    // Get sizes
    $sizes = get_terms('pa_size', array(
        'hide_empty' => false,
    ));
    $content = '
	<div class="kosma-filter">
	<div class="size-filter isotope-filters">
		<div class="size-title">Size</div>
		<div class="size-item" id="pa_size">';
    foreach ($sizes as $size) {
        $value = "'" . $size->name . "'";
        $content .= '<a href="#" data-filter="' . $size->name . '">' . $size->name . "</a>";
    }
    $content .= '<a href="#" data-filter="*">All</a>';
    $content .= '</div></div>';
    // Get color
    $colors = get_terms('pa_color', array(
        'hide_empty' => false,
    ));
    $content .= '<div class="other-filter">
		<div class="color-filter isotope-filters">
			<div class="color-title">Color</div>
			<div class="color-item" id="pa_color">';
    foreach ($colors as $color) {
        $term = get_term_by('name', $color->name, 'pa_color');

        if ($term) {
            $type = get_metadata('term', $term->term_id, 'pa_color_swatches_id_type', true);
//            echo $term->term_id.":".$type."---------------------><br>";
            if ($type == 'photo') {
                $image = get_metadata('term', $term->term_id, 'pa_color_swatches_id_photo', true);
                $image_link = wp_get_attachment_image_src($image, 'swatches_image_size')[0];
                $content .= '<a href="#" data-filter="' . $color->name . '"><img src="'.$image_link.'" alt="thumbnail"
                     class="wp-post-image swatch-photopa_color_ swatch-img kosma-swatch-img" width="32"
                     height="32"></a>';

            } else {
                $c = get_metadata('term', $term->term_id, 'pa_color_swatches_id_color', true);
                if($c!="")
                $content .= '<div class="kosma-color-item"  ><a href="#" data-filter="' . $color->name . '" style="text-indent:-9999px;width:32px;height:32px;background-color:'.$c.';" class="swatch-anchor kosma-swatch-color" >black</a></div>';
            }
        } else {

        }
    }
    $content .= '</div>
		</div>
		<div class="order-by">
			<select class="kosma-sort-by" id="kosma-sort-by">
				<option>Sort by</option>
				<option value="price">Price</option>
				<option value="name">Name</option>
			</select>
		</div>
	</div>';
    // FORM
    $content .= '<form id="kosma-filter" method="GET">
			<input type="hidden" id="kosma-filter-color" name="color" value=""></input>
			<input type="hidden" id="kosma-filter-size" name="size" value=""></input>';
    foreach ($_GET as $key => $value) {
        if ($key != 'color' && $key != 'size')
            $content .= '<input type="hidden" name="' . $key . '" value="' . $value . '"></input>';
    }
    $content .= '</form>
	';
    return $content;
}
add_shortcode('kosma_filter', 'kosma_filter_shortcode');

//Add choose language shortcod
function choose_language_shortcode($atts, $content = null)
{
    $content = '';
    $languages = icl_get_languages('skip_missing=0');
    if (1 < count($languages)) {
        foreach ($languages as $l) {
            $content .= '<option value="' . $l['code'] . '">' . $l['translated_name'] . '</option>';
        }
    }
    return '<select id="kosma-choose-language">' . $content . '</select><button class="custom-link btn btn-default btn-circle btn-icon-left" id="select-language">Go</button>';
}
add_shortcode('kosma_languages', 'choose_language_shortcode');

//Mapping integrate shortcod with Visual composer
add_action('vc_before_init', 'kosma_integrateWithVC');
function kosma_integrateWithVC()
{
    vc_map(
        array(
            "name" => __("Kosma color filter", "Kosma color filter"), // add a name
            "base" => "color_filter", // bind with our shortcode
            "content_element" => true, // set this parameter when element will has a content
            "is_container" => false, // set this param when you need to add a content element in this element
            // Here starts the definition of array with parameters of our compnent
            "params" => array(
                array(
                    "type" => "textfield", // it will bind a textfield in WP
                    "heading" => __("Title", "Kosma color filter"),
                    "param_name" => "title",
                )
            )
        ));
    vc_map(
        array(
            "name" => __("Kosma size filter", "Kosma size filter"), // add a name
            "base" => "size_filter", // bind with our shortcode
            "content_element" => true, // set this parameter when element will has a content
            "is_container" => false, // set this param when you need to add a content element in this element
            // Here starts the definition of array with parameters of our compnent
            "params" => array(
                array(
                    "type" => "textfield", // it will bind a textfield in WP
                    "heading" => __("Title", "Kosma size filter"),
                    "param_name" => "title",
                )
            )
        ));
    vc_map(
        array(
            "name" => __("Kosma order by", "Kosma order by"), // add a name
            "base" => "order_by", // bind with our shortcode
            "content_element" => true, // set this parameter when element will has a content
            "is_container" => false, // set this param when you need to add a content element in this element
            // Here starts the definition of array with parameters of our compnent
            "params" => array(
                array(
                    "type" => "textfield", // it will bind a textfield in WP
                    "heading" => __("Title", "Kosma order by"),
                    "param_name" => "title",
                )
            )
        )
    );
    vc_map(
        array(
            "name" => __("Kosma filter", "Kosma filter"), // add a name
            "base" => "kosma_filter", // bind with our shortcode
            "content_element" => true, // set this parameter when element will has a content
            "is_container" => false, // set this param when you need to add a content element in this element
            // Here starts the definition of array with parameters of our compnent
            "params" => array(
                array(
                    "type" => "textfield", // it will bind a textfield in WP
                    "heading" => __("Title", "Kosma filter"),
                    "param_name" => "title",
                )
            )
        )
    );
    vc_map(
        array(
            "name" => __("Choose languages", "Choose languages"), // add a name
            "base" => "kosma_languages", // bind with our shortcode
            "content_element" => true, // set this parameter when element will has a content
            "is_container" => false, // set this param when you need to add a content element in this element
            // Here starts the definition of array with parameters of our compnent
            "params" => array(
                array(
                    "type" => "textfield", // it will bind a textfield in WP
                    "heading" => __("Title", "Kosma choose languages"),
                    "param_name" => "title",
                )
            )
        )
    );
}

//Add query filter
function kosma_filter_action($q)
{
    $attributes['relation'] = 'AND';
    if (isset($_GET['color']) && $_GET['color'] != null)
        $attributes[] =
            array(
                'taxonomy' => 'pa_color',
                'field' => 'slug',
                'terms' => $_GET['color'],
            );
    if (isset($_GET['size']) && $_GET['size'] != null)
        $attributes[] =
            array(
                'taxonomy' => 'pa_size',
                'field' => 'slug',
                'terms' => $_GET['size'],
            );

    $q->set('tax_query', $attributes);
}
add_action('woocommerce_product_query', 'kosma_filter_action');

function compareUrls($a, $b) {
    $a = parse_url($a, PHP_URL_HOST);
    $b = parse_url($b, PHP_URL_HOST);

    return trimWord($a) === trimWord($b);
}

function trimWord($str) {
    if (stripos($str, 'www.') === 0) {
        return substr($str, 4);
    }
    return $str;
}
//Check the language
//function check_language()
//{
//    if (!isset($_COOKIE['kosma-choose-language'])) {
//        if (!is_page('welcome')) {
//            setcookie("kosma-last-referal", "https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"], time() + (86400 * 30), "/");
//            wp_redirect(home_url('/welcome/'));
//            exit();
//        }
//
//    } else {
//       if (isset($_COOKIE['kosma-choose-language'])) {
//           do_action('wpml_switch_language', $_COOKIE['kosma-choose-language']);
//           global $sitepress;
//           $sitepress->switch_lang($_COOKIE['kosma-choose-language'], true);
//        }
//    }
//}
//add_action('template_redirect', 'check_language');
//
////Change language
//function kosma_change_language($query)
//{
//    if (isset($_COOKIE['kosma-choose-language'])) {
//       do_action('wpml_switch_language', $_COOKIE['kosma-choose-language']);
//       global $sitepress;
//       $sitepress->switch_lang($_COOKIE['kosma-choose-language'], true);
//    }
//}

//Add flag
add_filter( 'nav_menu_link_attributes', 'add_flag_before_store', 10, 3 );
function add_flag_before_store( $atts, $item, $args ) {
    $menu_item = 'kosma-store';
    if (in_array($menu_item, $item->classes)) {
        $atts['data-store'] = get_locale();
    }
    return $atts;
}
// add_action('pre_get_posts', 'kosma_change_language');

//Replage payment icon
function replacePayPalIcon($iconUrl)
{
    $icon = "<img src='" . dirname(get_bloginfo('stylesheet_url')) . "/imgs/paypal.png" . "'>";
    apply_filters('woocommerce_gateway_icon', $icon, 'stripe');
    return dirname(get_bloginfo('stylesheet_url')) . "/imgs/paypal.png";
}
add_filter('woocommerce_paypal_icon', 'replacePayPalIcon');

// function filter_gateways($gateways){
//     global $woocommerce;
//     if(get_locale()=='en_US'){
//         unset($gateways['EG_Onepay_Gateway']);
//     }

//     if(get_locale()=='vi'){
//         unset($gateways['stripe']);
//     }
//     return $gateways;
// }
// add_filter('woocommerce_available_payment_gateways','filter_gateways',1);
// define the woocommerce_gateway_icon callback

function isa_extended_stripe_icon() {
    $icon = '';
    $icon .= '<img src="'. site_url() .'/wp-content/themes/uncode-child/imgs/paypal.png" alt="Stripe Payment Gateway" />';
    return $icon;
}
add_filter( 'woocommerce_stripe_icon', 'isa_extended_stripe_icon' );

// Our hooked in function - $fields is passed via the filter!
function custom_override_checkout_fields($fields)
{
    //die();
    $countries_obj   = new WC_Countries();
    $countries   = $countries_obj->__get('countries');
//
//    if(ICL_LANGUAGE_CODE == 'en'){
//        $onlyCountry["US"] = $countries["US"];
//    } else if(ICL_LANGUAGE_CODE == 'vi'){
//        $onlyCountry["VN"] = $countries["VN"];
//    }

    $fields['billing']['billing_first_name']['placeholder'] = 'First Name';
    $fields['billing']['billing_last_name']['placeholder'] = 'Last Name';
    $fields['billing']['billing_company']['placeholder'] = 'Company Name';
    $fields['billing']['billing_email']['placeholder'] = 'Email Address';
    $fields['billing']['billing_phone']['placeholder'] = 'Phone Number';
    $fields['billing']['billing_city']['placeholder'] = 'Town / City';
//    $fields['billing']['billing_country'] = array(
//        'label' => '',
//        'type' => 'select',
//        'placeholder' => _x('Country', 'placeholder', 'woocommerce'),
//        'required' => true,
//        'options'    => $onlyCountry
//    );
    $fields['billing']['billing_postcode']['placeholder'] = 'Postcode / ZIP';

    $fields['shipping']['shipping_first_name']['placeholder'] = 'First Name';
    $fields['shipping']['shipping_last_name']['placeholder'] = 'Last Name';
    $fields['shipping']['shipping_company']['placeholder'] = 'Company Name';
    $fields['shipping']['shipping_email']['placeholder'] = 'Email Address';
    $fields['shipping']['shipping_phone']['placeholder'] = 'Phone Number';
    $fields['shipping']['shipping_city']['placeholder'] = 'Town / City';
//    $fields['shipping']['shipping_country'] = array(
//        'label' => '',
//        'type' => 'select',
//        'placeholder' => _x('Country', 'placeholder', 'woocommerce'),
//        'required' => true,
//        'options'    => $onlyCountry
//    );
    $fields['shipping']['shipping_postcode']['placeholder'] = 'Postcode / ZIP';

    return $fields;
}
add_filter('woocommerce_checkout_fields', 'custom_override_checkout_fields');

//Load shipping method
function loadShippingMethod()
{
    $packages = WC()->shipping->get_packages();

    foreach ($packages as $i => $package) {
        $chosen_method = isset(WC()->session->chosen_shipping_methods[$i]) ? WC()->session->chosen_shipping_methods[$i] : '';
        $product_names = array();

        if (sizeof($packages) > 1) {
            foreach ($package['contents'] as $item_id => $values) {
                $product_names[] = $values['data']->get_title() . ' &times;' . $values['quantity'];
            }
        }
        wc_get_template('cart/cart-shipping.php', array(
            'package' => $package,
            'available_methods' => $package['rates'],
            'show_package_details' => sizeof($packages) > 1,
            'package_details' => implode(', ', $product_names),
            'package_name' => apply_filters('woocommerce_shipping_package_name', sprintf(_n('Shipping', 'Shipping %d', ($i + 1), 'woocommerce'), ($i + 1)), $i, $package),
            'index' => $i,
            'chosen_method' => $chosen_method
        ));
    }
}
add_action('kosma_load_shipping_method', 'loadShippingMethod');

//Load shipping method
function loadCurrentShippingMethod()
{
    $packages = WC()->shipping->get_packages();

    foreach ($packages as $i => $package) {
        $chosen_method = isset(WC()->session->chosen_shipping_methods[$i]) ? WC()->session->chosen_shipping_methods[$i] : '';
        $product_names = array();

        if (sizeof($packages) > 1) {
            foreach ($package['contents'] as $item_id => $values) {
                $product_names[] = $values['data']->get_title() . ' &times;' . $values['quantity'];
            }
        }
        $available_methods = $package['rates'];
        foreach ($available_methods as $method) {
            if ((string)$method->id === (string)$chosen_method) {
                $label = "";
                if ($method->cost > 0) {
                    if (WC()->cart->tax_display_cart == 'excl') {
                        $label .= wc_price($method->cost);
                        if ($method->get_shipping_tax() > 0 && WC()->cart->prices_include_tax) {
                            $label .= ' <small class="tax_label">' . WC()->countries->ex_tax_or_vat() . '</small>';
                        }
                    } else {
                        $label .= wc_price($method->cost + $method->get_shipping_tax());
                        if ($method->get_shipping_tax() > 0 && !WC()->cart->prices_include_tax) {
                            $label .= ' <small class="tax_label">' . WC()->countries->inc_tax_or_vat() . '</small>';
                        }
                    }
                } else $label = wc_price(0);
                echo '<tr class="shipping" id="kosma-shipinfo">
	                    <th >Shipping</th>
	                    <th colspan="2">' . $method->get_label() . '</th>
	                    <td >' . $label . '</td>
                    </tr>';
            }
        }
    }
}
add_action('kosma_load_current_shipping_method', 'loadCurrentShippingMethod');

function add_google_api_key(){
    return '&key=AIzaSyBO9WQJIIwtIAB8aUL0PgHPPdIR2BnlyVs';
}
add_filter('woogoogad_gg_api_other_parameters','add_google_api_key');

//Rename tabs name
function woo_rename_tabs( $tabs ) {
    $tabs['description']['title'] = __( 'Description' );		// Rename the description tab
    $tabs['reviews']['title'] = __( 'Review' );				// Rename the reviews tab
    $tabs['additional_information']['title'] = __( 'Size Guide' );	// Rename the additional information tab
    return $tabs;
}
add_filter( 'woocommerce_product_tabs', 'woo_rename_tabs', 98 );

if (!function_exists('write_log')) {
    function write_log ( $log )  {
        if ( true === WP_DEBUG ) {
            if ( is_array( $log ) || is_object( $log ) ) {
                error_log( print_r( $log, true ) );
            } else {
                error_log( $log );
            }
        }
    }
}

// After registration redirect user to the custom page
// function custom_registration_redirect() {
//     // wp_logout();
//     return home_url('/thank-you-for-registration/');
// }
// add_action('woocommerce_registration_redirect', 'custom_registration_redirect', 2);

function wpml_filter_gateways($gateways){
     if(ICL_LANGUAGE_CODE == 'en'){
        //Checks if the selected language is English.
        unset($gateways['cod']); //"remove" cash on delivery
        unset($gateways['bacs']); //"remove" bank transfer
        unset($gateways['EG_Onepay_Gateway']); //"remove" onepay
        unset($gateways['EG_Onepay_Gateway_US']); //"remove" onepay
     } else if(ICL_LANGUAGE_CODE == 'vi'){
        //Checks if the selected language is Vietnamese.
        unset($gateways['stripe']); //"remove" stripe
        unset($gateways['paypal']); //"remove" paypal
     }

     return $gateways; //returns the other payment methods.
}
add_filter('woocommerce_available_payment_gateways','wpml_filter_gateways',1);

// disable shipping for vietnam
//function unset_woocommerce_shipping_methods_when_ids ( $rates, $package ) {
//    global $woocommerce;
//    // loop through the cart checking the products
//    if(ICL_LANGUAGE_CODE == 'vi'){
//        $rates = array();
//    }
//
//    return $rates;
//}
//add_filter( 'woocommerce_package_rates', 'unset_woocommerce_shipping_methods_when_ids', 10 ,2 );