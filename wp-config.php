<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'centricwear');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'uUqZl=wRK-7ZoL19kqer u2V+qA`0q6i(Mm~+TG}8tZUqac /x}lv)i%EYbxDac|');
define('SECURE_AUTH_KEY',  'HA5%(q_w3C!</T)4<`Y$/:N,tua(=<gT%MV@!iThFbD]$} -9 +TNaGU+8,>774z');
define('LOGGED_IN_KEY',    'zCZYK{W|3+~6e{7ujZRj-~2 -b9jH)-v_</voU`$0uWI )ZahoZr;`E]:K}5]xRi');
define('NONCE_KEY',        '|<e(CS9|R|&QHS----;lkyBPXztHyn]Y/Ci|C+-E@l3d!/=/j|rQ_4eTa=wQn3hs');
define('AUTH_SALT',        '<=[gR5g?,+yun)^/wDd}uPbBP0n!%wZ<a+J?p|d{bR|_knLOcC+n{]saM%`@sU|V');
define('SECURE_AUTH_SALT', 'Zl*L+kB-M1S#+12F-&qDT+ +2y#rg~CCc<u0}-Jr+,Y;&-6#E8w-;y3jZK}2|d&N');
define('LOGGED_IN_SALT',   'w,iBi^;gGHMb%nKu $rKdmNd`b!u<  dr&~|72liFHzdY;2V7#y[;QwJ5+_ s/c3');
define('NONCE_SALT',       'a>hPAe~xh?GOxo+a[Fe>y$w4.d!Ql!`r|[.GMs:I@ZAPN_.Uv)oK j{^WO$+G4Pu');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'cw_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* multisite */
define( 'WP_ALLOW_MULTISITE', true );
define('MULTISITE', true);
define('SUBDOMAIN_INSTALL', false);
define('DOMAIN_CURRENT_SITE', 'www.centricwear.com');
define('PATH_CURRENT_SITE', '/');
define('SITE_ID_CURRENT_SITE', 1);
define('BLOG_ID_CURRENT_SITE', 1);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
